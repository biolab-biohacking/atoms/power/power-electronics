# **Atom sub-Group**

<h1>power-electronics </h1>

power driver based on the several projects of biolab

This is a kit that intents to drive power devices like motor, heating resistance, etc

The input will be a 12V power input that feeds the arduino and the device

From the kit a connector goes to arduno and from arduino we get the control signal (Dx) and the the ground

The purpose of this kit, is to have a device that does only one task **driving a device**, and it can be used for a new experiment

A branch will be created for creating a chained version of this kit, that in the end wil create a similar situation like the incubator

The incubator driving two devices will chain two kits, this is just an idea to be tested

# Render image

![Render](/photos/power-rendered.png)

# PCB

![PCB](/photos/power-pcb-00.png)
