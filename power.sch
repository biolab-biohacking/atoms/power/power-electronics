EESchema Schematic File Version 4
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Power Control"
Date "2021-06-20"
Rev "Rev 0.9.TBR"
Comp "Luis Dinis - altLab"
Comment1 ""
Comment2 ""
Comment3 "based on https://www.instructables.com/Incubadora-Garagem-FabLab-Biohackacademy/"
Comment4 ""
$EndDescr
$Comp
L dk_Transistors-FETs-MOSFETs-Single:IRF540NPBF Q2
U 1 1 60AFA7DF
P 7125 2100
F 0 "Q2" H 7233 2153 60  0000 L CNN
F 1 "IRF-Fan" H 7233 2047 60  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7325 2300 60  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 7325 2400 60  0001 L CNN
F 4 "IRF540NPBF-ND" H 7325 2500 60  0001 L CNN "Digi-Key_PN"
F 5 "IRF540NPBF" H 7325 2600 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 7325 2700 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 7325 2800 60  0001 L CNN "Family"
F 8 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 7325 2900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/infineon-technologies/IRF540NPBF/IRF540NPBF-ND/811869" H 7325 3000 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 100V 33A TO-220AB" H 7325 3100 60  0001 L CNN "Description"
F 11 "Infineon Technologies" H 7325 3200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 7325 3300 60  0001 L CNN "Status"
	1    7125 2100
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J2
U 1 1 60AFB970
P 1050 2200
F 0 "J2" H 942 1875 50  0000 C CNN
F 1 "FromArduino" H 942 1966 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 1050 2200 50  0001 C CNN
F 3 "~" H 1050 2200 50  0001 C CNN
	1    1050 2200
	-1   0    0    -1  
$EndComp
$Comp
L pspice:R R7
U 1 1 60AFF4A8
P 6325 2200
F 0 "R7" V 6120 2200 50  0000 C CNN
F 1 "Fan-R" V 6211 2200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0411_L9.9mm_D3.6mm_P12.70mm_Horizontal" H 6325 2200 50  0001 C CNN
F 3 "~" H 6325 2200 50  0001 C CNN
	1    6325 2200
	0    1    1    0   
$EndComp
$Comp
L pspice:R R8
U 1 1 60B01F9C
P 7125 1375
F 0 "R8" V 6920 1375 50  0000 C CNN
F 1 "Fan" V 7011 1375 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 7125 1375 50  0001 C CNN
F 3 "~" H 7125 1375 50  0001 C CNN
	1    7125 1375
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 60B02D22
P 800 1125
F 0 "J1" H 692 800 50  0000 C CNN
F 1 "ToArduino" H 692 891 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 800 1125 50  0001 C CNN
F 3 "~" H 800 1125 50  0001 C CNN
	1    800  1125
	-1   0    0    1   
$EndComp
Wire Notes Line
	525  1575 2275 1575
$Comp
L pspice:DIODE D1
U 1 1 60B0CC70
P 7600 1400
F 0 "D1" H 7600 1135 50  0000 C CNN
F 1 "DIODE" H 7600 1226 50  0000 C CNN
F 2 "Diode_THT:D_DO-15_P5.08mm_Vertical_KathodeUp" H 7600 1400 50  0001 C CNN
F 3 "~" H 7600 1400 50  0001 C CNN
	1    7600 1400
	0    -1   -1   0   
$EndComp
Wire Notes Line
	3050 675  3050 2425
Wire Notes Line
	3050 2425 7925 2425
Wire Notes Line
	7925 2425 7925 675 
Wire Notes Line
	7925 675  3050 675 
Wire Notes Line
	5650 675  5650 2425
Wire Wire Line
	6075 2200 6075 2300
Connection ~ 6075 2300
Wire Wire Line
	6075 2300 7125 2300
Wire Wire Line
	6675 1925 6675 2200
Wire Wire Line
	6575 2200 6675 2200
Connection ~ 6675 2200
Wire Wire Line
	6675 2200 6825 2200
Wire Notes Line
	525  1925 525  2650
Wire Notes Line
	525  2650 2275 2650
Wire Notes Line
	2275 2650 2275 1925
Wire Notes Line
	2275 1925 525  1925
Text Notes 3050 625  0    50   ~ 0
Heating Control
Text Notes 525  575  0    50   ~ 0
Power Heating/Fan
Text Notes 525  1850 0    50   ~ 0
Power - Signal
Text Notes 5650 625  0    50   ~ 0
Fan Control
Text Notes 550  1550 0    35   Italic 7
To Vin/GND of Arduino
Text Notes 6425 1825 0    35   Italic 7
Pin D5
Wire Notes Line
	2275 600  525  600 
Wire Notes Line
	525  600  525  1575
Wire Notes Line
	2275 600  2275 1575
Wire Wire Line
	1400 1025 1000 1025
$Comp
L Connector:Conn_01x02_Female J3
U 1 1 60B67757
P 1400 650
F 0 "J3" H 1175 350 50  0000 C CNN
F 1 "Power12V" H 1200 425 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_CUI_PJ-063AH_Horizontal" H 1400 650 50  0001 C CNN
F 3 "~" H 1400 650 50  0001 C CNN
	1    1400 650 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1400 850  1400 1025
Wire Wire Line
	1000 1125 1500 1125
Wire Wire Line
	1500 850  1500 1125
Connection ~ 1500 1125
Wire Wire Line
	7125 1625 7125 1675
Wire Wire Line
	7600 1200 7600 1125
Wire Wire Line
	7600 1125 7125 1125
Connection ~ 7125 1125
Wire Wire Line
	7600 1600 7600 1675
Wire Wire Line
	7600 1675 7125 1675
Connection ~ 7125 1675
Wire Wire Line
	7125 1675 7125 1900
Wire Wire Line
	1500 1125 7125 1125
Wire Wire Line
	1250 2300 6075 2300
Wire Wire Line
	6675 1925 2675 1925
Wire Wire Line
	2675 1925 2675 2200
Wire Wire Line
	2675 2200 1250 2200
$EndSCHEMATC
